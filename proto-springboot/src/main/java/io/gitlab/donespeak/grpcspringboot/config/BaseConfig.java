 package io.gitlab.donespeak.grpcspringboot.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.protobuf.ProtobufHttpMessageConverter;

import io.gitlab.donespeak.grpcspringboot.entity.Book;
import io.gitlab.donespeak.grpcspringboot.mapper.BookMapper;

/**
 * @author Guanrong Yang
 * @date 2019/07/08
 */
 @Configuration
public class BaseConfig {

     @Bean
     public ProtobufHttpMessageConverter protobufHttpMessageConverter() {
         return new ProtobufHttpMessageConverter();
     }
     
     @Bean
     public BookMapper bookMapper() {
         return new BookMapper() {

            @Override
            public List<Book> listBooks() {
                List<Book> books = new ArrayList<Book>();
                Book book1 = new Book();
                book1.setIsbn(10001);
                Book book2 = new Book();
                book2.setIsbn(10004);
                
                books.add(book1);
                books.add(book2);
                
                return books;
            }
         };
     }
}