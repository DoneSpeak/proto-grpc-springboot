 package io.gitlab.donespeak.grpcspringboot.entity;

import lombok.Data;

/**
 * @author Guanrong Yang
 * @date 2019/07/08
 */
@Data
public class Book {

    private long isbn;
    private String name;
    private String author;
    private double price;
    private String publishTime;
    private String caption;
    private String tags;
    private boolean isDeleted;
}
