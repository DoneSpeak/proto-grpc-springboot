 package io.gitlab.donespeak.exchange;

import org.junit.Test;

import com.googlecode.protobuf.format.JsonFormat;

import io.gitlab.donespeak.grpcspringboot.exchange.BookVo;

/**
  * https://github.com/bivas/protobuf-java-format
  * 
 * @author Guanrong Yang
 * @date 2019/07/09
 */
public class ProtobufJavaFormatTest {

    @Test
    public void toJsonTest() {
        BookVo.BookResponse book = BookVo.BookResponse.newBuilder()
            .setIsbn(1545)
            .setName("The Book Name")
            .build();
        
        String jsonFormat = new JsonFormat().printToString(book);
        
        System.out.println(jsonFormat);
    }
}
