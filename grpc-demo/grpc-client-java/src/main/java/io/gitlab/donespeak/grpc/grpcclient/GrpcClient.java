 package io.gitlab.donespeak.grpc.grpcclient;

import io.gitlab.donespeak.grpc.exchange.HelloRequest;
import io.gitlab.donespeak.grpc.exchange.HelloResponse;
import io.gitlab.donespeak.grpc.exchange.HelloServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

/**
 * @author Guanrong Yang
 * @date 2019/07/12
 */
public class GrpcClient {

    public static void main(String[] args) {
        String grpcServerName = "localhost";
        int grpcServerPort = 8080;
        
        ManagedChannel channel = ManagedChannelBuilder.forAddress(grpcServerName, grpcServerPort)
            .usePlaintext()
            .build();
        
        HelloServiceGrpc.HelloServiceBlockingStub stub
            = HelloServiceGrpc.newBlockingStub(channel);
        
        HelloResponse helloResponse = stub.hello(HelloRequest.newBuilder()
            .setFirstName("GR")
            .setLastName("Yang")
            .build());
        
        System.out.println("Response received from server:\n" + helloResponse);
        
        channel.shutdown();
    }
}
