 package io.gitlab.donespeak.grpc.grpcserver.grpcservice;

import java.io.IOException;

import io.grpc.Server;
import io.grpc.ServerBuilder;

/**
 * @author Guanrong Yang
 * @date 2019/07/12
 */
public class GrpcServer {
    public static void main(String[] args) throws IOException, InterruptedException {
        int serverPort = 8080;
        Server server = ServerBuilder.forPort(serverPort)
            .addService(new HelloServiceImpl())
            // .addService()
            .build();
        
        System.out.println("Starting server...");
        
        server.start();
        
        System.out.println("Server started!");
        
        server.awaitTermination();
    }
}
