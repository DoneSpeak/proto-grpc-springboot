package io.gitlab.donespeak.grpc.grpcserver.grpcservice;

import io.gitlab.donespeak.grpc.exchange.HelloResponse;
import io.gitlab.donespeak.grpc.exchange.HelloRequest;
import io.gitlab.donespeak.grpc.exchange.HelloServiceGrpc.HelloServiceImplBase;
import io.grpc.stub.StreamObserver;

/**
 * @author Guanrong Yang
 * @date 2019/07/12
 */
public class HelloServiceImpl extends HelloServiceImplBase {

    @Override
    public void hello(HelloRequest request, StreamObserver<HelloResponse> responseObserver) {
        System.out.println("Request received from client:\n" + request);
        String greeting = new StringBuilder().append("Hello, ")
            .append(request.getFirstName())
            .append(" ")
            .append(request.getLastName())
            .toString();
        
        HelloResponse response = HelloResponse.newBuilder()
            .setGreeting(greeting)
            .build();
        
        responseObserver.onNext(response);
        responseObserver.onCompleted();
    }
}
