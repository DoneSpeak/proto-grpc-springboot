gRPC Server
===

## 项目配置

参考 [gRPC-Java - An RPC library and framework](https://github.com/grpc/grpc-java/blob/master/README.md) 可以完成整合 gRPC + Protobuf到Java项目中的功能。  

项目实现参考：  
- [eugenp/tutorials/grpc](https://github.com/eugenp/tutorials/tree/master/grpc)
- [Introduction to gRPC](https://www.baeldung.com/grpc-introduction)