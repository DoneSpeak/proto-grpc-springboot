 package client;

import com.googlecode.protobuf.format.JsonFormat;

import io.gitlab.donespeak.grpcspringboot.exchange.BookVo;
import io.gitlab.donespeak.grpcspringboot.exchange.service.BookServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;

/**
 * @author Guanrong Yang
 * @date 2019/07/19
 */
public class GrpcClient {

    public static void main(String[] args) {
        String grpcServerName = "localhost";
        int grpcServerPort = 50051;
        
        ManagedChannel channel = ManagedChannelBuilder.forAddress(grpcServerName, grpcServerPort)
            .usePlaintext()
            .build();
        
        BookServiceGrpc.BookServiceBlockingStub bookService
            = BookServiceGrpc.newBlockingStub(channel);
        
        BookVo.BookRequest request = BookVo.BookRequest.newBuilder()
            .setIsbn(15545452)
            .setName("Fdddd")
            .setAuthor("FFFF")
            .build();
        
        BookVo.BookResponse book = bookService.createBook(request);
        
        System.out.println("### book: " + new JsonFormat().printToString(book));
        
        channel.shutdown();
    }
}
