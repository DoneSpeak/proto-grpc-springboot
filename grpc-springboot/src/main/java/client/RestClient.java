 package client;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpMessage;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.AbstractHttpMessage;
import org.apache.http.util.EntityUtils;
import org.apache.tomcat.util.http.parser.MediaType;

import com.google.protobuf.GeneratedMessageV3;
import com.google.protobuf.InvalidProtocolBufferException;
import com.googlecode.protobuf.format.JsonFormat;

import io.gitlab.donespeak.grpcspringboot.exchange.BookVo;

/**
 * @author Guanrong Yang
 * @date 2019/07/19
 */
public class RestClient {

    public static void main(String[] args) {
        BookVo.BookRequest request = BookVo.BookRequest.newBuilder()
            .setIsbn(45454)
            .setName("dfff")
            .build();
        String url = "http://localhost:8080/books/454545455";
        
        byte[] bytes = get(request, url);
        if(bytes != null) {
            try {
                BookVo.BookResponse bookReponse = BookVo.BookResponse.parseFrom(bytes);
                System.out.println("### book: " + new JsonFormat().printToString(bookReponse));
            } catch (InvalidProtocolBufferException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
    }
    
    public static byte[] get(GeneratedMessageV3 requstBody, String url) {
     // 创建Httpclient对象
        CloseableHttpClient httpclient = HttpClients.createDefault();
        
        HttpGet httpGet = new HttpGet(url);
        
        setHttpHeaders(httpGet);
        
        CloseableHttpResponse reponse = execute(httpclient, httpGet);
        
        if(reponse.getStatusLine().getStatusCode() == 200) {
            try {
                byte[] bytes = EntityUtils.toByteArray(reponse.getEntity());
                System.out.println("### OK");
                return bytes;
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        System.out.println("### Fail");
        return null;
    }
    
    public static CloseableHttpResponse post(GeneratedMessageV3 requstBody, String url) {
        // 创建Httpclient对象
        CloseableHttpClient httpclient = HttpClients.createDefault();
        // 创建http POST请求
        HttpPost httpPost = new HttpPost(url);
        
        setHttpHeaders(httpPost);
        
        // 将请求实体设置到httpPost对象中
        httpPost.setEntity(new ByteArrayEntity(requstBody.toByteArray()));
//        //伪装浏览器
//        httpPost.setHeader("User-Agent",
//                "Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/56.0.2924.87 Safari/537.36");
        return execute(httpclient, httpPost);
    }
    
    private static CloseableHttpResponse execute(CloseableHttpClient httpclient, 
            HttpUriRequest httpUriRequest) {
        CloseableHttpResponse response = null;
        try {
            // 执行请求
            response = httpclient.execute(httpUriRequest);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return response;
    }
    
    private static void close(CloseableHttpClient httpclient) {
        if(httpclient != null) {
            try {
                httpclient.close();
            } catch (IOException e) {
                 e.printStackTrace();
            }
        }
    }
    
    private static void setHttpHeaders(HttpMessage httpMessage) {
        httpMessage.setHeader("Content-Type", "application/x-protobuf");
        httpMessage.setHeader("Accept", "application/x-protobuf");
    }
}
