 package io.gitlab.donespeak.grpcspringboot.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.gitlab.donespeak.grpcspringboot.entity.Book;
import io.gitlab.donespeak.grpcspringboot.mapper.BookMapper;
import io.gitlab.donespeak.grpcspringboot.service.BookService;

/**
 * @author Guanrong Yang
 * @date 2019/07/08
 */
@Service("bookService")
public class BookServiceImpl implements BookService {
    
    @Autowired
    private BookMapper bookMapper;

    @Override
    public List<Book> listBooks() {
        return bookMapper.listBooks();
    }

}
