 package io.gitlab.donespeak.grpcspringboot.mapper;

import java.util.List;

import io.gitlab.donespeak.grpcspringboot.entity.Book;

/**
 * @author Guanrong Yang
 * @date 2019/07/08
 */
public interface BookMapper {

    List<Book> listBooks();
}
