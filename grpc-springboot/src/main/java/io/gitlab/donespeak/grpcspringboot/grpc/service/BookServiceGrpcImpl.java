package io.gitlab.donespeak.grpcspringboot.grpc.service;

import org.springframework.stereotype.Component;

import com.googlecode.protobuf.format.JsonFormat;

import io.gitlab.donespeak.grpcspringboot.exchange.BookVo;
import io.gitlab.donespeak.grpcspringboot.exchange.service.BookServiceGrpc;
import io.grpc.stub.StreamObserver;

/**
 * @author Guanrong Yang
 * @date 2019/07/19
 */
@Component
public class BookServiceGrpcImpl extends BookServiceGrpc.BookServiceImplBase {
    
    public void createBook(BookVo.BookRequest request,
        StreamObserver<BookVo.BookResponse> responseObserver) {
        
        System.out.println(new JsonFormat().printToString(request));
        
        BookVo.BookResponse reponse = BookVo.BookResponse.newBuilder()
            .setName("A book")
            .setAuthor("Adam")
            .setIsbn(request.getIsbn())
            .build();
        
        responseObserver.onNext(reponse);
        responseObserver.onCompleted();
    }
}
