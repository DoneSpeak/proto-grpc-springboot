package io.gitlab.donespeak.grpcspringboot.grpc;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import io.grpc.BindableService;
import io.grpc.Server;
import io.grpc.ServerBuilder;
import io.grpc.util.TransmitStatusRuntimeExceptionInterceptor;
import lombok.extern.slf4j.Slf4j;

/**
 * @author Guanrong Yang
 * @date 2019/07/19
 */
@Component
@Slf4j
public class GrpcServerInitializer implements ApplicationRunner {

    private List<BindableService> services;

    private int port;

    @Autowired
    public GrpcServerInitializer(List<BindableService> services, @Value("${grpc.server.port}") int port) {
        this.port = port;
        this.services = services == null ? new ArrayList<BindableService>() : services;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {

        ServerBuilder<?> serverBuilder = ServerBuilder.forPort(port);

        for (BindableService bindableService : services) {
            serverBuilder.addService(bindableService);
        }

        Server server = serverBuilder.build();
        serverBuilder.intercept(TransmitStatusRuntimeExceptionInterceptor.instance());
        server.start();

        showInfo();

        startDaemonAwaitThread(server);
    }

    private void showInfo() {
        StringBuilder gprcServerInfo = new StringBuilder();
        gprcServerInfo.append("Grpc Server is started, port: " + port + "\n");
        gprcServerInfo.append("The following Grpc Services is available: \n");
        for (BindableService service : services) {
            gprcServerInfo.append("\t" + service.getClass().getSimpleName() + "\n");
        }
        log.info("Gprc Server Info:\n" + gprcServerInfo);
    }

    private void startDaemonAwaitThread(Server server) {
        Thread awaitThread = new Thread(() -> {
            try {
                server.awaitTermination();
            } catch (InterruptedException ignore) {

            }
        });
        awaitThread.setDaemon(false);
        awaitThread.start();
    }
}
