package io.gitlab.donespeak.grpcspringboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GrpcspringbootApplication {

	public static void main(String[] args) {
		SpringApplication.run(GrpcspringbootApplication.class, args);
	}
}