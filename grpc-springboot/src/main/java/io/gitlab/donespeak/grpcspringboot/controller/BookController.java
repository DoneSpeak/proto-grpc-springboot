 package io.gitlab.donespeak.grpcspringboot.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.gitlab.donespeak.grpcspringboot.entity.Book;
import io.gitlab.donespeak.grpcspringboot.exchange.BookVo;
import io.gitlab.donespeak.grpcspringboot.mapper.BookMapper;
import io.gitlab.donespeak.grpcspringboot.service.BookService;

/**
 * @author Guanrong Yang
 * @date 2019/07/08
 */
@RestController
@RequestMapping(value="/books", consumes="application/x-protobuf", produces="application/x-protobuf")
public class BookController {

    @Autowired
    public BookService bookService;
    
    @GetMapping("/{isbn:\\d+}")
    public BookVo.BookResponse getBook(@PathVariable long isbn) {
        System.out.println("### BookController#getBook: " + isbn);
        return BookVo.BookResponse.newBuilder()
            // 9780201485417
            .setIsbn(isbn)
            .setName("The Art of Computer Programming")
            .setAuthor("Donald Knuth")
            .build();
    }
    
    @GetMapping("")
    public BookVo.BookResponseList listBooks(BookVo.BookRequest bookRequest) {
        System.out.println("### BookController#listBooks: " + bookRequest);
        
        List<Book> books = bookService.listBooks();
        List<BookVo.BookResponse> bookResponses = new ArrayList<BookVo.BookResponse>();
        for(Book b: books) {
            BookVo.BookResponse bookResponse = BookVo.BookResponse.newBuilder()
                .setIsbn(b.getIsbn())
                .build();
            bookResponses.add(bookResponse);
        }
        BookVo.BookResponseList response = BookVo.BookResponseList.newBuilder()
            .addAllBooks(bookResponses)
            .build();
        
        return response;
    }
    
    @PostMapping("")
    public BookVo.BookResponse createBook(@RequestBody BookVo.BookResponse book) {
        System.out.println("### BookController#createBook: " + book);
        
        return book;
    }
    
    @DeleteMapping("/{isbn:\\d+}")
    public void deleteBook(@PathVariable long isbn) {
        System.out.println("### BookController#deleteBook: " + isbn);
    }
    
    @PutMapping("/{isbn:\\d+}")
    public BookVo.BookResponse putBook(BookVo.BookResponse book) {
        System.out.println("### BookController#putBook: " + book);
        return book;
    }
    
    @PatchMapping("/{isbn:\\d+}")
    public BookVo.BookResponse patchBook(BookVo.BookResponse book) {
        System.out.println("### BookController#patchBook: " + book);
        return book;
    }
}