package io.gitlab.donespeak.grpcspringboot;

import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.protobuf.ProtobufHttpMessageConverter;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;

import io.gitlab.donespeak.grpcspringboot.exchange.BookVo;

/**
 * 原本一直参考：[]() 进行实现，但一直失败，所以按照springboot的说明文档进行实现了。
 * [TestRestTemplate](https://docs.spring.io/spring-boot/docs/2.0.0.RELEASE/reference/htmlsingle/#boot-features-rest-templates-test-utility)
 * []()
 * 
 * @author Guanrong Yang
 * @date 2019/07/09
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
// SpringApplicationConfiguration 已经被 SpringBootTest 取代
// @WebAppConfiguration
public class DemoApplicationTest {

    @TestConfiguration
    static class RestClientConfiguration {

        /**
         * 
         * @param protobufHttpMessageConverter
         *            {@link BaseConfig#protobufHttpMessageConverter}
         * @return
         */
        @Bean
        public RestTemplateBuilder restTemplateBuilder(ProtobufHttpMessageConverter protobufHttpMessageConverter) {
            System.out.println("&&& DemoApplicationTests#RestClientConfiguration#restTemplateBuilder");
            return new RestTemplateBuilder().additionalMessageConverters(Arrays.asList(protobufHttpMessageConverter));
        }
    }

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getByIdTest() {

        HttpEntity<BookVo.BookResponse> entity = new HttpEntity<>(getHttpHeaders());

        // 使用exchange方法代替getForObject方法才可以设置Header
        ResponseEntity<BookVo.BookResponse> bookResponse =
            restTemplate.exchange("/books/4545", HttpMethod.GET, entity, BookVo.BookResponse.class);

        System.out.println("&&& RestClientConfiguration#getByIdTest: " + bookResponse);
        Assert.assertEquals(bookResponse.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void listTest() {
        HttpEntity<BookVo.BookResponseList> entity = new HttpEntity<>(getHttpHeaders());

        // 使用exchange方法代替getForObject方法才可以设置Header
        ResponseEntity<BookVo.BookResponseList> bookResponse =
            restTemplate.exchange("/books", HttpMethod.GET, entity, BookVo.BookResponseList.class);

        System.out.println("&&& RestClientConfiguration#listTest: " + bookResponse);
        Assert.assertEquals(bookResponse.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void postTest() {
        BookVo.BookResponse book = BookVo.BookResponse.newBuilder().setIsbn(155488).setName("The book").build();

        HttpEntity<BookVo.BookResponse> entity = new HttpEntity<>(book, getHttpHeaders());

        // 使用exchange方法代替getForObject方法才可以设置Header
        ResponseEntity<BookVo.BookResponse> bookResponse =
            restTemplate.exchange("/books", HttpMethod.POST, entity, BookVo.BookResponse.class);

        System.out.println("&&& RestClientConfiguration#postTest: " + bookResponse);
        Assert.assertEquals(bookResponse.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void putTest() {
        BookVo.BookResponse book = BookVo.BookResponse.newBuilder().setIsbn(155488).setName("The book").build();

        HttpEntity<BookVo.BookResponse> entity = new HttpEntity<>(book, getHttpHeaders());

        // 使用exchange方法代替getForObject方法才可以设置Header
        ResponseEntity<BookVo.BookResponse> bookResponse =
            restTemplate.exchange("/books/741852", HttpMethod.PUT, entity, BookVo.BookResponse.class);

        System.out.println("&&& RestClientConfiguration#putTest: " + bookResponse);
        Assert.assertEquals(bookResponse.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void patchTest() {
        BookVo.BookResponse book = BookVo.BookResponse.newBuilder().setIsbn(155488).setName("The book").build();

        HttpEntity<BookVo.BookResponse> entity = new HttpEntity<>(book, getHttpHeaders());

        // 使用exchange方法代替getForObject方法才可以设置Header
        ResponseEntity<BookVo.BookResponse> bookResponse =
            restTemplate.exchange("/books/852963", HttpMethod.PATCH, entity, BookVo.BookResponse.class);

        System.out.println("&&& RestClientConfiguration#patchTest: " + bookResponse);
        Assert.assertEquals(bookResponse.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void deleteTest() {
        HttpEntity<BookVo.BookResponse> entity = new HttpEntity<>(getHttpHeaders());

        // 使用exchange方法代替getForObject方法才可以设置Header
        ResponseEntity<BookVo.BookResponse> bookResponse =
            restTemplate.exchange("/books/99745632", HttpMethod.DELETE, entity, BookVo.BookResponse.class);

        System.out.println("&&& RestClientConfiguration#deleteTest: " + bookResponse);

        Assert.assertEquals(bookResponse.getStatusCode(), HttpStatus.OK);
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        MediaType contentType = MediaType.parseMediaType("application/x-protobuf");
        headers.setContentType(contentType);
        headers.add("Accept", contentType.toString());

        return headers;
    }
}
